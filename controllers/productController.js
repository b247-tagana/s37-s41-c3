const Product = require("../models/Product");

// AddProduct
module.exports.addProduct = (data) => {

	
	let newProduct = new Product({
		category : data.product.category,
		image : data.product.image,
		name : data.product.name,
		description : data.product.description,
		quantity: data.product.quantity,
		price : data.product.price
	});

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		} else {
			return {
				message: "New product successfully created!"
			}
		};
	});

	// if(data.isAdmin){

	// }

	// let message = Promise.resolve({
	// 	message: "User must be an Admin to access this!"
	// })

	return message.then((value) => {
		return value;
	})
};

// UpdateProduct
module.exports.updateProduct = (reqParams, reqBody) => {
  return Product.findById(reqParams.productId)
    .then((product) => {
      if (!product) {
        return false; // Product not found
      } else {
        // Update the existing product with the new details
        if (reqBody.category) {
          product.category = reqBody.category;
        }
        if (reqBody.image) {
          product.image = reqBody.image;
        }
        if (reqBody.name) {
          product.name = reqBody.name;
        }
        if (reqBody.description) {
          product.description = reqBody.description;
        }
        if (reqBody.quantity) {
          product.quantity = reqBody.quantity;
        }
        if (reqBody.price) {
          product.price = reqBody.price;
        }

        return product.save()
          .then((updatedProduct) => {
            return true;
          })
          .catch((error) => {
            return false; // Failed to save the updated product
          });
      }
    })
    .catch((error) => {
      return false; // Error occurred while finding the product
    });
};


// AllProduct
module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

// All ActiveProduct
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	});
};

// All Inactive
module.exports.getAllInactive = () => {
	return Product.find({isActive : false}).then(result => {
		return result;
	});
};

// Get Product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// Controller for DeleteProduct
module.exports.deleteProduct = async function (params) {
    try {
      // Assuming you have a database connection and a Product model defined
      const deletedProduct = await Product.findByIdAndDelete(params.productId);
      if (!deletedProduct) {
        return { success: false, message: 'Product not found' };
      }
      return { success: true, message: 'Product deleted successfully' };
    } catch (error) {
      return { success: false, message: error.message };
  }
};

// ArchiveProduct
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
}

// ActivateProduct
module.exports.activateProduct = (reqParams) => {

	let updateActiveField = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
}