const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

const Product = require("../models/Product");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});
};

module.exports.loginUser = (reqBody) => {

	// We use the "findOne" method instead of the "find" method which return all records that match the search criteria
	// The "findOne" method returns the first record in the collection that matches the search criteria
	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){
			return false;
		
		// User exist
		} else {

			// The "compareScync" method is used to compate a non encrypted password from the login form to the excrypted password retrived from the database and it returns "true" or "false" value depending on the result
			// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect){
				
				// Generate an access token
				// return { access : auth.createAccessToken(result)}
				return {id : result._id}
			} else {
				return false;
			};
		};
	});
};

// Get User
module.exports.getProfile = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		return result;
	});
};

// Get AllUser
module.exports.getAllUser = () => {
	return User.find({}).then(result => {
		return result;
	});
};

// Controller for DeleteProfile
module.exports.deleteProfile = async function (params) {
    try {
      // Assuming you have a database connection and a User model defined
      const deleteProfile = await User.findByIdAndDelete(params.userId);
      if (!deleteProfile) {
        return { success: false, message: 'User not found' };
      }
      return { success: true, message: 'User deleted successfully' };
    } catch (error) {
      return { success: false, message: error.message };
  }
};

// SetUserAsAdmin
module.exports.setAsAdmin = (reqParams) => {

	let updateActiveField = {
		isAdmin: true
	};

	return User.findByIdAndUpdate(reqParams.userId, updateActiveField).then((user, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
}