const Order = require("../models/Order");
const User = require("../models/User");

module.exports.checkout = (data) => {

  let newOrder = new Order({
    userId: data.userId,
    products: data.products,
    totalAmount: data.totalAmount,
    purchasedOn: new Date(),
  });

  return newOrder
    .save()
    .then((order) => {
      return {
        message: "New order successfully created!",
      };
    })
    .catch((error) => {
      return {
        message: "An error occurred while saving the order.",
      };
    });
};

// AllOrder
module.exports.getAllOrder = () => {
  return Order.find({}).then(result => {
    return result;
  });
};

module.exports.getOrder = (userId) => {
  return Order.find({ userId: userId })
    .then(result => {
      return result;
    })
    .catch(error => {
      throw new Error("An error occurred while fetching the user's orders.");
    });
};

// Controller for DeleteOrder
module.exports.deleteOrder = async function (params) {
    try {
      const deleteOrder = await Order.findByIdAndDelete(params.orderId);
      if (!deleteOrder) {
        return { success: false, message: 'Order not found' };
      }
      return { success: true, message: 'Order deleted successfully' };
    } catch (error) {
      return { success: false, message: error.message };
  }
};