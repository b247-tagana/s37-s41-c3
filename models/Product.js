const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	category : {
		type : String,
		required : [true, "Product category is required"]
	},
	image : {
		type : String,
		required : [true, "Product image is required"]
	},
	name : {
		type : String,
		required : [true, "Product name is required"]
	},
	description : {
		type : String,
		required : [true, "Product description is required"]
	},
	price : {
		type : Number,
		required : [true, "Product price is required"]
	},
	quantity : {
		type : Number,
		required : [true, "Product quantity is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	enquiries : [
		{
			userId : {
				type : String,
				required : [true, "User ID is required"]
			},
			enquiryOn : {
				type : Date,
				default : new Date()
			}
		}
	]
});

module.exports = mongoose.model("Product", productSchema);
