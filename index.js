const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

const app = express();

const port = 4000;

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://tjtagana:6wopfEZlAmfnw2rw@zuitt-bootcamp1.x4smuop.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// CORS stands for Cross-Origin Resources Sharing. 
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Declaring main routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/order", orderRoutes);

app.listen(port, () => console.log(`API is now online on port ${port}`));