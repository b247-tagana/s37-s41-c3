const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");

// Authenticated non-admin user (checkout)
router.post("/checkout", (req, res) => {
	orderController.checkout(req.body).then(
		resultFromController => res.send(resultFromController));
});


module.exports = router;

// Route for AllOrder
router.get("/all", (req, res) => {

	orderController.getAllOrder().then(resultFromController => res.send(resultFromController));
});

// Route for User's Order
router.get("/:userId/order", (req, res) => {
  orderController.getOrder(req.params.userId)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.send({ message: error.message }));
});

// Route for DeleteOrder
router.delete("/:orderId/delete", (req, res) => {

	orderController.deleteOrder(req.params).then(resultFromController => res.send(resultFromController));
});