const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");

// Route for AddProduct
router.post("/", (req, res) => {

	const data = {
		product : req.body,
		// isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for UpdateProduct
router.put("/:productId/update", (req, res) => {

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for AllProduct
router.get("/all", (req, res) => {

	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});

// Route for All ActiveProduct
router.get("/active", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for All InactiveProduct
router.get("/inactive", (req, res) => {

	productController.getAllInactive().then(resultFromController => res.send(resultFromController));
});

// Route for GetProduct
router.get("/:productId/details", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for DeleteProduct
router.delete("/:productId/delete", (req, res) => {

	productController.deleteProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for ArchiveProduct
router.put("/:productId/archive", (req, res) => {

	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;

// Route for ActivateProduct
router.put("/:productId/activate", (req, res) => {

	productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;