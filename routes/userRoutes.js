const express = require("express");
const router = express.Router();

const auth = require("../auth");
const userController = require("../controllers/userController");

// Check the email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});

// Register a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.get("/:userId/details", (req, res) => {

	userController.getProfile(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for AllUser
router.get("/all", (req, res) => {
	userController.getAllUser().then(resultFromController => res.send(resultFromController));
});

// Route for DeleteUser
router.delete("/:userId/delete", (req, res) => {

	userController.deleteProfile(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for SetAdminUser
router.put("/:userId/setAsAdmin", (req, res) => {

	userController.setAsAdmin(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;